import axios from "axios";
import { useState } from "react";
import { useRef } from "react";

export default function WeddingDelete(){
    const [wedding,setWedding] = useState({})
    const idInput = useRef(null)

    async function deleteWedding(event){
        const weddingId = idInput.current.value;
        console.log(weddingId);
        const response = await axios.delete(`http://localhost:3000/wedding/${weddingId}`)
        const weddingData = response.data;
        console.log(weddingData)
        setWedding(weddingData)
    }
    
    return(<div>
        <button onClick={deleteWedding}>Delete Wedding by ID</button>
        <input ref={idInput}/>
    
     </div>)
}