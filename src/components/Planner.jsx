import React from "react";
import ExpenseTable from "./expense-table";
import WeddingTable from "./wedding-table";
import WeddingForm from "./wedding-form";
import ExpenseForm from "./expense-form";
import WeddingDetails from "./weddingbyid";
import WeddingDelete from "./delete-wed";
import ExpenseDelete from "./delete-expense";
import ExpenseDetails from "./expensebyWedId";

function Planner() {
  return (
    <div className="planner">
      <div class="container">
        <div class="row align-items-center my-10">
          <div class="col-lg-7">
            <img
              class="img-fluid rounded mb-4 mb-lg-0"
              src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQKFxWmolcP-0WiJyOU5_kZK59-3LUbzPLSuA&usqp=CAU/900x400"
              alt=""
            />
          </div>
          <div class="col-lg-5">
            <h1 class="font-weight-dark">Wedding Planner</h1>
            <p>
              Add a new Wedding or Expense record, for by ID or delete a record below.
            </p>
            <WeddingTable></WeddingTable>
            <hr></hr>
            <ExpenseTable></ExpenseTable>
            <hr></hr>
            <WeddingForm></WeddingForm> 
            <br></br>
            <ExpenseForm></ExpenseForm>
            <hr></hr>
            <WeddingDetails></WeddingDetails>
            <hr></hr>
            <ExpenseDetails></ExpenseDetails>
            <hr></hr>
            <WeddingDelete></WeddingDelete>
            <hr></hr>
            <ExpenseDelete></ExpenseDelete>
          </div>
        </div>
      </div>
    </div>
  );
}

export default Planner;