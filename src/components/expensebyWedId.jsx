import axios from "axios";
import { useState } from "react";
import { useRef } from "react";

export default function ExpenseDetails(){

    const [expense,setExpense] = useState([])
    const idInput = useRef(null)

    async function getExpenseByWeddingId(event){
        const weddingId = idInput.current.value;
        const response = await axios.get(`http://localhost:3000/weddings/${weddingId}/expenses`)
        const expenseData = response.data;
        console.log(expenseData)
        setExpense(expenseData)
    }
    

    const tableRows = expense.map(e => <tr> <td>{e.expenseId}</td><td>{e.reason}</td><td>{e.amount}</td></tr>)
    return(<div>
        <button onClick={getExpenseByWeddingId}>Get Expenses by Wedding ID</button>
        <input ref={idInput}/>
        <table> 
        <thead><th>Expense ID</th><th>Reason</th><th>Amount</th></thead>
        {tableRows}
     </table>
     </div>)
}