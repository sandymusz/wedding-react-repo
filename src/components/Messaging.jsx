import React from "react";

function Messaging() {
  return (
    <div className="messaging">
      <div class="container">
        <div class="row align-items-center my-5">
          <div class="col-lg-7">
            <img
              class="img-fluid rounded mb-4 mb-lg-0"
              src="https://i.insider.com/5b9ab9883cccd123008b4602?width=750&format=jpeg&auto=webp/900x400"
              alt=""
            />
          </div>
          <div class="col-lg-5">
            <h1 class="font-weight-light">Messaging</h1>
            <p>
              Coming Soon!
            </p>
            <p>
              We are working hard to build a messaging service for you to communicate with your team. Please come back soon. 
            </p>
          </div>
        </div>
      </div>
    </div>
  );
}

export default Messaging;