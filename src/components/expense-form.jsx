import axios from "axios";
import { useRef } from "react"

export default function ExpenseForm(){

    const reasonInput = useRef(null);
    const weddingIdInput = useRef(null);
    const amountInput = useRef(null);
    
    

    async function addExpense(){

        const expense = {
            expenseId:0,
            reason:reasonInput.current.value,
            weddingId:weddingIdInput.current.value,
            amount:Number(amountInput.current.value),
        }

        const response = await axios.post('http://localhost:3000/expense',expense)
        alert("You created a new expense");
    }
    return(<div>

        <h3>Expense Creation Form</h3>

        <input placeholder="reason" ref={reasonInput}></input>
        <input placeholder="weddingId" ref={weddingIdInput}></input>
        <input placeholder="amount" ref={amountInput} type="number"></input>
        <button onClick={addExpense}>Add Expense</button>

    </div>)
}