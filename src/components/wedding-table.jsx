import axios from "axios";
import { useState } from "react";

export default function WeddingTable(){
    const [weddings,setWeddings] = useState([])

    async function getAllWeddings(event){
        const response = await axios.get('http://localhost:3000/weddings')
        const weddingData = response.data;
        setWeddings(weddingData)
    }
    const tableRows = weddings.map(w => <tr> <td>{w.weddingId}</td><td>{w.name}</td><td>{w.location}</td><td>{w.date}</td><td>{w.budget}</td></tr>)
    return(<div>
        <button onClick={getAllWeddings}>Get Weddings</button>
        <table> 
        <thead><th>Wedding ID</th><th>Name</th><th>Location</th><th>Date</th><th>Budget</th></thead>
        {tableRows}
     </table>
     </div>)
}


