import axios from "axios";
import { useState } from "react";
import { useRef } from "react";

export default function WeddingDetails(){
    const [wedding,setWedding] = useState({})
    const idInput = useRef(null)

    async function getWeddingById(event){
        const weddingId = idInput.current.value;
        console.log(weddingId);
        const response = await axios.get(`http://localhost:3000/weddings/${weddingId}`)
        const weddingData = response.data;
        console.log(weddingData)
        setWedding(weddingData)
    }
    
    return(<div>
        <button onClick={getWeddingById}>Get Wedding by ID</button>
        <input ref={idInput}/>
        <table><thead><th>Wedding ID</th><th>Name</th><th>Location</th><th>Date</th><th>Budget</th></thead>
        </table>
        <h6>{wedding.weddingId} {wedding.name} {wedding.location} {wedding.date} {wedding.budget}</h6>
     </div>)
}