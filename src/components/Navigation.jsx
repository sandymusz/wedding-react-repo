import React from "react";
import { Link, withRouter } from "react-router-dom";

function Navigation(props) {
  return (
    <div className="navigation">
      <nav class="navbar navbar-expand navbar-dark bg-dark">
        <div class="container">
          <Link class="navbar-brand" to="/">
            Ever After Wedding Planning
          </Link>

          <div>
            <ul class="navbar-nav ml-auto">
            <li
                class={`nav-item  ${
                  props.location.pathname === "/" ? "active" : ""
                }`}
              >
                <Link class="nav-link" to="/">
                  Login
                  <span class="sr-only">(current)</span>
                </Link>
              </li>
              <li
                class={`nav-item  ${
                  props.location.pathname === "/messaging" ? "active" : ""
                }`}
              >
                <Link class="nav-link" to="/messaging">
                  Messaging
                </Link>
              </li>
              <li
                class={`nav-item  ${
                  props.location.pathname === "/planner" ? "active" : ""
                }`}
              >
                <Link class="nav-link" to="/planner">
                  Planner
                </Link>
              </li>
            </ul>
          </div>
        </div>
      </nav>
    </div>
  );
}

export default withRouter(Navigation);