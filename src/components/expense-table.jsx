import axios from "axios";
import { useState } from "react";

export default function ExpenseTable(){
    const [expenses,setExpenses] = useState([])


    async function getAllExpenses(event){
        const response = await axios.get('http://localhost:3000/expenses')
        const expenseData = response.data;
        setExpenses(expenseData)
    }


    const tableRows = expenses.map(e => <tr> <td>{e.weddingId} {e.expenseId}</td><td>{e.reason}</td><td>{e.amount}</td></tr>)
    return(<div>
        <button onClick={getAllExpenses}>Get Expenses</button> 
        <table>   
        <thead><th>Wedding ID</th><th>Expense ID</th><th>Reason</th><th>Amount</th></thead>
        {tableRows}
     </table>
    
     </div>)
     
}