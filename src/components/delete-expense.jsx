import axios from "axios";
import { useState } from "react";
import { useRef } from "react";

export default function ExpenseDelete(){
    const [expense,setExpense] = useState({})
    const idInput = useRef(null)

    async function deleteExpense(event){
        const expenseId = idInput.current.value;
        console.log(expenseId);
        const response = await axios.delete(`http://localhost:3000/expenses/${expenseId}`)
        const expenseData = response.data;
        console.log(expenseData)
        setExpense(expenseData)
    }
    
    return(<div>
        <button onClick={deleteExpense}>Delete Expense by ID</button>
        <input ref={idInput}/>
    
     </div>)
}