import React from "react";


function Login() {
  return (
    <div className="login">
      <div class="container">
        <div class="row align-items-center my-5">
          <div class="col-lg-7">
            <img
              class="img-fluid rounded mb-4 mb-lg-0"
              src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSf9herP3xmZxcUJ4nMXWic8SjslUxpH2Avzg&usqp=CAU/900x400"
              alt=""
            />
          </div>
          <div class="col-lg-5">
            <h1 class="font-weight-light">Login</h1>
            <p>
              We are so glad your part of our team! Please sign in below.
            </p>
            <form>
            <label>
             <p>Username</p>
             <input type="text" />
             </label>
             <label>
             <p>Password</p>
             <input type="password" />
             </label>
             <div>
             <button type="submit">Submit</button>
            </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  );
}
export default Login;