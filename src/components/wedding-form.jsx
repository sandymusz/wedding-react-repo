import axios from "axios";
import { useRef } from "react"

export default function WeddingForm(){

    const nameInput = useRef(null);
    const locationInput = useRef(null);
    const dateInput = useRef(null);
    const budgetInput = useRef(null);

    async function addWedding(){

        const wedding = {
            weddingId:0,
            name:nameInput.current.value,
            location:locationInput.current.value,
            date:dateInput.current.value,
            budget:Number(budgetInput.current.value),
        }

        const response = await axios.post('http://localhost:3000/weddings',wedding)
        alert("You created a new wedding");
    }
    return(<div>

        <h3>Wedding Creation Form</h3>

        <input placeholder="name" ref={nameInput}></input>
        <input placeholder="location" ref={locationInput}></input>
        <input placeholder="date" ref={dateInput}></input>
        <input placeholder="budget" ref={budgetInput} type="number"></input>
        <button onClick={addWedding}>Add Wedding</button>

    </div>)
}