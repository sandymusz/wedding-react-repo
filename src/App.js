import React from "react";
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
import { Navigation, Footer, Login, Messaging, Planner } from "./components";
function App() {
  return (
    <div className="App">
      <Router>
        <Navigation />
        <Switch>
          <Route path="/" exact component={() => <Login />} />
          <Route path="/messaging" exact component={() => <Messaging />} />
          <Route path="/planner" exact component={() => <Planner />} />
        </Switch>
        <Footer />
      </Router>
    </div>
  );
}

export default App;
